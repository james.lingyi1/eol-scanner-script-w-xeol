#!/bin/bash

input_file="repo_tags.txt"
output_file="SDLC_scan_results.txt"

> "$output_file"

echo "REPOSITORY,TAG,NAME,VERSION,EOL,DAYS EOL,TYPE" >> "$output_file"

# Read each repository from the input file
while IFS= read -r line; do
    repository=$(echo "$line" | cut -d':' -f1)
    tag=$(echo "$line" | cut -d':' -f2-)

    echo "Scanning $repository with tag $tag"

    # Run the xeol tool with a 3 minutes timeout ELSE YOU WASTE 8 HOURS SCANNING 1000 IMAGES
    output=$(timeout 180s xeol registry:"$line" 2>&1)

    if [ $? -eq 124 ]; then
        # If timeout occurs, append TIMEOUT to the output file
        echo "$repository,$tag,,,TIMEOUT,," >> "$output_file"
    elif echo "$output" | grep -q "✅ no EOL software has been found"; then
        # If no EOL software found, append this info to the output file
        echo "$repository,$tag,,,no EOL software found,," >> "$output_file"
    else
        # For all other outputs, filter, format, and append to the output file
        echo "$output" | grep -v -E '^\[|\✔' | awk -v repo="$repository" -v tag="$tag" 'NR>1 {print repo","tag","$1","$2","$3","$4","$5}' >> "$output_file"
    fi
done < "$input_file"

echo "Scan complete. Results saved to $output_file."
