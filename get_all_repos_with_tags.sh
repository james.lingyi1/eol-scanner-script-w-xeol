#!/bin/bash

output_file="repo_tags.txt"

> "$output_file"

repos=$(aws ecr describe-repositories --query 'repositories[*].repositoryName' --output text)

for repo in $repos; do
    tags=$(aws ecr describe-images --repository-name "$repo" --query 'imageDetails[*].imageTags[]' --output text)
    
    if [ -n "$tags" ]; then
        for tag in $tags; do
            echo "044093426884.dkr.ecr.ap-southeast-1.amazonaws.com/${repo}:${tag}" >> "$output_file"
        done
    fi
done

echo "Finished listing ECR repositories and tags to $output_file."
